library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity val_out_sync is
	port (
		clk_sync : in std_logic; --slow clk
		rst_n : in std_logic;
		val_unsinced : in std_logic;
		val_sinced : out std_logic
	);
end entity val_out_sync;

architecture RTL of val_out_sync is
	
	signal ff1: std_logic;
	
begin
	
	sync : process (clk_sync, rst_n) is
	begin
		if rst_n = '0' then
			
		elsif falling_edge(clk_sync) then
			ff1  <= val_unsinced;
			val_sinced  <= ff1;
			
		end if;
	end process sync;
	

end architecture RTL;
