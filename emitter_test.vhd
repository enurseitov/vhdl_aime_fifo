library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity emitter_test is


end entity emitter_test;

architecture RTL of emitter_test is
	
	component emitter
		port(
			clk   : out std_logic;
			rst_n : in  std_logic;
			val   : out std_logic;
			data  : out std_logic_vector(7 downto 0);
			en    : in  std_logic
		);
	end component emitter;
	signal clk : std_logic;
	signal rst_n : std_logic;
	signal val : std_logic;
	signal data : std_logic_vector(7 downto 0);
	signal en : std_logic;
	
begin
	
	EM : entity work.emitter
		port map(
			clk   => clk,
			rst_n => rst_n,
			val   => val,
			data  => data,
			en    => en
		);
		
		en  <=   '0', '1' after 275 ns, '0' after 1025 ns, '1' after 1325 ns, '0' after 2025 ns;
		rst_n  <= '0', '1' after 100 ns;

end architecture RTL;
