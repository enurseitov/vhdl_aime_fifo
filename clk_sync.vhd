library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_sync is
	port (
		clk_in : in std_logic;
		clk_sync : in std_logic;
		clk_out : out std_logic;
		rst_n : in std_logic
	);
end entity clk_sync;

architecture RTL of clk_sync is
	
begin
sync : process (clk_sync, rst_n) is
begin
	if rst_n = '0' then
		clk_out <= '0';
		
	elsif rising_edge(clk_sync) then
		clk_out <= clk_in;
		
	end if;
end process sync;

end architecture RTL;
