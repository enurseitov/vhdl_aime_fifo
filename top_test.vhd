library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_test is
			generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
end entity top_test;

architecture RTL of top_test is
	component top
		generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
		port(
			clk_wr, clk_rd : in  std_logic;
			rst_n          : in  std_logic;
			data_in        : in  std_logic_vector(7 downto 0);
			data_out       : out std_logic_vector(7 downto 0);
			val_in         : in  std_logic;
			val_out        : out std_logic;
			en_emitter     : out std_logic
		);
	end component top;

	component emitter
		port(
			clk   : out std_logic;
			rst_n : in  std_logic;
			val   : out std_logic;
			data  : out std_logic_vector(7 downto 0);
			en    : in  std_logic
		);
	end component emitter;

	signal clk_wr, clk_rd : std_logic;
	signal val_in : std_logic;
	signal data_emitter, data_out : std_logic_vector(7 downto 0);
	signal en_emitter: std_logic;
	signal rst_n : std_logic;
	signal val_out : std_logic;
	

begin
	EMIT : entity work.emitter
		port map(
			clk   => clk_wr,
			rst_n => rst_n,
			val   => val_in,
			data  => data_emitter,
			en    => en_emitter
		);

	TP : entity work.top
		generic map(
			DATA_WIDTH => DATA_WIDTH,
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			clk_wr     => clk_wr,
			clk_rd     => clk_rd,
			rst_n      => rst_n,
			data_in    => data_emitter,
			data_out   => data_out,
			val_in     => val_in,
			val_out    => val_out,
			en_emitter => en_emitter
		);
		
rd_clock_driver : process
	constant period : time := 1011 ns; -- 1 MHz imperfect
begin
	clk_rd <= '0';
	wait for period / 2;
	clk_rd <= '1';
	wait for period / 2;
end process rd_clock_driver;

rst_n  <= '0', '1' after 100 ns;
		

end architecture RTL;
