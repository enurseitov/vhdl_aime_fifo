library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
	generic(
		DATA_WIDTH : integer := 8;
		ADDR_WIDTH : integer := 8
	);
	port(
		clk_wr, clk_rd : in  std_logic;
		rst_n          : in  std_logic;
		data_in        : in  std_logic_vector(7 downto 0);
		data_out       : out std_logic_vector(7 downto 0);
		val_in         : in  std_logic;
		val_out        : out std_logic;
		en_emitter     : out std_logic
	);
end entity top;

architecture RTL of top is
	signal wr_addr, rd_addr                       : std_logic_vector(7 downto 0);
	signal en_rd                                  : std_logic;
	signal ram_cs_1, ram_cs_2, ram_wr_1, ram_wr_2 : std_logic;
	signal datain_int, dataout_int                : std_logic_vector(7 downto 0) := (others => '0');
	signal clk_rd_synced                          : std_logic;
	signal dummy1, dummy2                         : std_logic_vector(7 downto 0) := (others => '0');
	signal val_out_int : std_logic;
	

	component io_controller
		port(
			clk              : in  std_logic;
			rst_n            : in  std_logic;
			wr_addr, rd_addr : in  std_logic_vector(7 downto 0);
			en_rd            : out std_logic;
			en_emitter       : out std_logic;
			val_out          : out std_logic
		);
	end component io_controller;

	component rd_controller
		generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
		port(
			clk               : in  std_logic;
			rst_n             : in  std_logic;
			ram_cs, ram_wr_en : out std_logic;
			en                : in  std_logic;
			rd_addr           : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
			datain            : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			dataout           : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component rd_controller;

	component wr_controller
		generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
		port(
			clk               : in  std_logic;
			rst_n             : in  std_logic;
			ram_cs, ram_wr_en : out std_logic;
			en                : in  std_logic;
			wr_addr           : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
			datain            : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			dataout           : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component wr_controller;

	component dpram
		generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
		port(
			addr1, addr2         : in  STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
			cs1, cs2             : in  STD_LOGIC;
			wr1, wr2             : in  STD_LOGIC;
			data_in1, data_in2   : in  STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
			data_out1, data_out2 : out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
			clk1, clk2           : in  STD_LOGIC
		);
	end component dpram;

	component clk_sync
		port(
			clk_in   : in  std_logic;
			clk_sync : in  std_logic;
			clk_out  : out std_logic;
			rst_n    : in  std_logic
		);
	end component clk_sync;
	
	component val_out_sync
		port(
			clk_sync     : in  std_logic;
			rst_n        : in  std_logic;
			val_unsinced : in  std_logic;
			val_sinced   : out std_logic
		);
	end component val_out_sync;

begin
	IO_CTR : entity work.io_controller
		port map(
			clk        => clk_wr,
			rst_n      => rst_n,
			wr_addr    => wr_addr,
			--wr_addr    => dummy1,
			rd_addr    => rd_addr,
			--rd_addr    => dummy2,
			en_rd      => en_rd,
			en_emitter => en_emitter,
			val_out    => val_out_int
		);

	WR_CTR : entity work.wr_controller
		generic map(
			DATA_WIDTH => DATA_WIDTH,
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			clk       => clk_wr,
			rst_n     => rst_n,
			ram_cs    => ram_cs_1,
			ram_wr_en => ram_wr_1,
			en        => val_in,
			wr_addr   => wr_addr,
			datain    => data_in,
			dataout   => datain_int
		);

	CLK_SYN : entity work.clk_sync
		port map(
			clk_in   => clk_rd,
			clk_sync => clk_wr,
			clk_out  => clk_rd_synced,
			rst_n    => rst_n
		);
		
	VALOUT_SYN : entity work.val_out_sync
		port map(
			clk_sync     => clk_rd_synced,
			rst_n        => rst_n,
			val_unsinced => val_out_int,
			val_sinced   => val_out
		);

	RD_CTR : entity work.rd_controller
		generic map(
			DATA_WIDTH => DATA_WIDTH,
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			clk       => clk_rd_synced,
			rst_n     => rst_n,
			ram_cs    => ram_cs_2,
			ram_wr_en => ram_wr_2,
			en        => en_rd,
			rd_addr   => rd_addr,
			datain    => dataout_int,
			dataout   => data_out
		);

	RAM : entity work.dpram
		generic map(
			DATA_WIDTH => DATA_WIDTH,
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			addr1     => wr_addr,
			addr2     => rd_addr,
			cs1       => ram_cs_1,
			cs2       => ram_cs_2,
			wr1       => ram_wr_1,
			wr2       => ram_wr_2,
			data_in1  => datain_int,
			data_in2  => dummy1,
			data_out1 => dummy2,
			data_out2 => dataout_int,
			clk1      => clk_wr,
			clk2      => clk_rd_synced
		);

end architecture RTL;
