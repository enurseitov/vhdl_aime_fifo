library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wr_controller is
	generic(DATA_WIDTH : integer := 8;
		    ADDR_WIDTH : integer := 8);
	port(
		clk               : in  std_logic;
		rst_n             : in  std_logic;
		ram_cs, ram_wr_en : out std_logic;
		en                : in  std_logic;
		wr_addr           : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
		datain : in std_logic_vector(DATA_WIDTH-1 downto 0);
		dataout : out std_logic_vector(DATA_WIDTH-1 downto 0)
				
	);
end entity wr_controller;

architecture RTL of wr_controller is
	signal addr : unsigned (ADDR_WIDTH - 1 downto 0) := (others => '0');
	
begin
	
	sync : process (clk, rst_n) is
	begin
		if rst_n = '0' then
			addr  <= (others => '0');
			
		elsif rising_edge(clk) then
			
			if en = '1' then
				addr  <= addr+1;
				ram_cs  <=  '1';
				ram_wr_en  <= '1';
			else
				ram_cs  <=  '0';
				ram_wr_en  <= '0';				
				
			end if;		
			
			dataout  <= datain;
			
		end if;
		
		
	end process sync;
	
wr_addr  <= std_logic_vector(addr);
	
end architecture RTL;
