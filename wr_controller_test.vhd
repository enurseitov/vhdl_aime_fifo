library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wr_controller_test is

end entity wr_controller_test;

architecture RTL of wr_controller_test is
	
	component wr_controller
		generic(
			DATA_WIDTH : integer := 8;
			ADDR_WIDTH : integer := 8
		);
		port(
			clk               : in  std_logic;
			rst_n             : in  std_logic;
			ram_cs, ram_wr_en : out std_logic;
			en                : in  std_logic;
			wr_addr           : out std_logic_vector(ADDR_WIDTH - 1 downto 0);
			datain            : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			dataout           : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component wr_controller;
	--signal DATA_WIDTH : integer;
	--signal ADDR_WIDTH : integer;
	signal clk : std_logic;
	signal rst_n : std_logic;
	signal ram_cs : std_logic;
	signal ram_wr_en : std_logic;
	signal en : std_logic;
	signal wr_addr : std_logic_vector(7 downto 0);
	signal datain : std_logic_vector( 7 downto 0);
	signal dataout : std_logic_vector(7 downto 0);
	
begin

WR_CONT : entity work.wr_controller
	generic map(
		DATA_WIDTH => 8,
		ADDR_WIDTH => 8
	)
	port map(
		clk       => clk,
		rst_n     => rst_n,
		ram_cs    => ram_cs,
		ram_wr_en => ram_wr_en,
		en        => en,
		wr_addr   => wr_addr,
		datain    => datain,
		dataout   => dataout
	);
	

clock_driver : process
	constant period : time := 20 ns;
begin
	clk <= '0';
	wait for period / 2;
	clk <= '1';
	wait for period / 2;
end process clock_driver;

rst_n <= '0', '1' after 100 ns;
datain  <= (others =>  '1');

en  <=  '0', '1' after 150 ns;



end architecture RTL;
