library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity emitter is
	port(
		clk   : out std_logic;
		rst_n : in  std_logic;
		val   : out std_logic;
		data  : out std_logic_vector(7 downto 0);
		en    : in  std_logic
	);
end entity emitter;

architecture RTL of emitter is
	signal clk_int : std_logic;
	signal count   : unsigned(7 downto 0) := (others => '0');
	signal ff_en   : std_logic;

begin
	name : process(clk_int, rst_n) is
	begin
		if rst_n = '0' then
			count <= (others => '0');

		elsif falling_edge(clk_int) then
			ff_en <= en;
			if ff_en = '1' then
				if count < 200 then
					count <= count + 1;
					val   <= '1';

				else
					val <= '0';

				end if;

			else
				val <= '0';

			end if;

		end if;
	end process name;

	clock_driver : process
		constant period : time := 50 ns; --20 MHz
	begin
		clk_int <= '0';
		wait for period / 2;
		clk_int <= '1';
		wait for period / 2;
	end process clock_driver;

	clk  <= clk_int;
	data <= std_logic_vector(count);

end architecture RTL;
