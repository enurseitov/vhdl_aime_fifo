library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;

entity io_controller is
	generic(
		constant margin      : integer := 3;
		constant buffer_size : integer := 16
	);
	port(
		clk              : in  std_logic;
		rst_n            : in  std_logic;
		wr_addr, rd_addr : in  std_logic_vector(7 downto 0);
		en_rd            : out std_logic;
		en_emitter       : out std_logic;
		val_out          : out std_logic
	);
end entity io_controller;

architecture RTL of io_controller is
	signal w_addr, r_addr, diff : integer;

	type state_type is (idle, ram_fill, ram_flush);
	SIGNAL state : state_type;

begin
	w_addr <= to_integer(unsigned(wr_addr));
	r_addr <= to_integer(unsigned(rd_addr));

	--	w_addr <= conv_integer(wr_addr);
	--	r_addr <= conv_integer(rd_addr);

	diff <= (w_addr - r_addr + 256) mod 256;

	name : process(clk, rst_n) is
	begin
		if rst_n = '0' then
			state      <= idle;
			en_emitter <= '1';
			val_out    <= '0';
			en_rd      <= '0';

		elsif rising_edge(clk) then
			case state is
				when idle =>
					if diff > 0 then
						state <= ram_fill;

						en_emitter <= '1';
						val_out    <= '1';
						en_rd      <= '1';

					end if;

				when ram_fill =>
					if diff > (buffer_size - margin) then
						state <= ram_flush;

						en_emitter <= '0';
						val_out    <= '1';
						en_rd      <= '1';
					end if;

					--no responce from emitter and buffer is empty	
					if diff = 0 then
						state <= idle;

						val_out <= '0';
						en_rd   <= '0';
					end if;

				when ram_flush =>
					if diff < margin then
						state      <= ram_fill;
						en_emitter <= '1';
					end if;

			end case;

		end if;

	end process name;

end architecture RTL;
